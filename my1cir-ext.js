// my1cir - personal customized simcir (http://www.d-project.com) extension

!function($s) {

  'use strict';

  var $ = $s.$;
  // unit size
  var unit = $s.unit;

  // blue/white
  var defaultLEDColor = '#0000ff';
  var defaultLEDBgColor = '#ffffff';
  var defaultSEGBgColor = '#000000';

  var multiplyColor = function() {
    var HEX = '0123456789abcdef';
    var toIColor = function(sColor) {
      if (!sColor) {
        return 0;
      }
      sColor = sColor.toLowerCase();
      if (sColor.match(/^#[0-9a-f]{3}$/i) ) {
        var iColor = 0;
        for (var i = 0; i < 6; i += 1) {
          iColor = (iColor << 4) | HEX.indexOf(sColor.charAt( (i >> 1) + 1) );
        }
        return iColor;
      } else if (sColor.match(/^#[0-9a-f]{6}$/i) ) {
        var iColor = 0;
        for (var i = 0; i < 6; i += 1) {
          iColor = (iColor << 4) | HEX.indexOf(sColor.charAt(i + 1) );
        }
        return iColor;
      }
      return 0;
    };
    var toSColor = function(iColor) {
      var sColor = '#';
      for (var i = 0; i < 6; i += 1) {
        sColor += HEX.charAt( (iColor >>> (5 - i) * 4) & 0x0f);
      }
      return sColor;
    };
    var toRGB = function(iColor) {
      return {
        r: (iColor >>> 16) & 0xff,
        g: (iColor >>> 8) & 0xff,
        b: iColor & 0xff};
    };
    var multiplyColor = function(iColor1, iColor2, ratio) {
      var c1 = toRGB(iColor1);
      var c2 = toRGB(iColor2);
      var mc = function(v1, v2, ratio) {
        return ~~Math.max(0, Math.min( (v1 - v2) * ratio + v2, 255) );
      };
      return (mc(c1.r, c2.r, ratio) << 16) |
        (mc(c1.g, c2.g, ratio) << 8) | mc(c1.b, c2.b, ratio);
    };
    return function(color1, color2, ratio) {
      return toSColor(multiplyColor(
          toIColor(color1), toIColor(color2), ratio) );
    };
  }();

  var vdcValue = 1;
  var refValue = 0;
  var vDiff = function(v,w) { return ((v!=null)&&(w!=null)&&(v>w))?1:0; }

  // register direct current source
  $s.registerDevice('VDC', function(device) {
    var node = device.addOutput(); node.setName('VDC');
    var super_createUI = device.createUI;
    device.createUI = function() {
      super_createUI();
      device.$ui.addClass('my1cir-vdc');
    };
    device.$ui.on('deviceAdd', function() {
      device.getOutputs()[0].setValue(vdcValue);
    });
    device.$ui.on('deviceRemove', function() {
      device.getOutputs()[0].setValue(null);
    });
  });

  // register direct current ground
  $s.registerDevice('GND', function(device) {
    var node = device.addOutput(); node.setName('GND');
    var super_createUI = device.createUI;
    device.createUI = function() {
      super_createUI();
      device.$ui.addClass('my1cir-gnd');
    };
    device.$ui.on('deviceAdd', function() {
      device.getOutputs()[0].setValue(refValue);
    });
    device.$ui.on('deviceRemove', function() {
      device.getOutputs()[0].setValue(null);
    });
  });

  // register simple LED
  $s.registerDevice('LED', function(device) {
    var in1 = device.addInput(); in1.setName('Anode');
    var in2 = device.addInput(); in2.setName('Cathode');
    var super_createUI = device.createUI;
    device.createUI = function() {
      super_createUI();
      var hiColor = device.deviceDef.color || defaultLEDColor;
      var bgColor = device.deviceDef.bgColor || defaultLEDBgColor;
      var loColor = multiplyColor(hiColor, bgColor, 0.25);
      var bLoColor = multiplyColor(hiColor, bgColor, 0.2);
      var bHiColor = multiplyColor(hiColor, bgColor, 0.8);
      var size = device.getSize();
      var $ledbase = $s.createSVGElement('circle').
        attr({cx: size.width / 2, cy: size.height / 2, r: size.width / 4}).
        attr('stroke', 'none').
        attr('fill', bLoColor);
      device.$ui.append($ledbase);
      var $led = $s.createSVGElement('circle').
        attr({cx: size.width / 2, cy: size.height / 2, r: size.width / 4 * 0.8}).
        attr('stroke', 'none').
        attr('fill', loColor);
      device.$ui.append($led);
      device.$ui.on('inputValueChange', function() {
        var temp = vDiff(in1.getValue(),in2.getValue());
        $ledbase.attr('fill',temp?bHiColor:bLoColor);
        $led.attr('fill',temp?hiColor:loColor);
      });
      device.doc = {
        params: [
          {name: 'color', type: 'string',
            defaultValue: defaultLEDColor,
            description: 'color in hexadecimal.'},
          {name: 'bgColor', type: 'string',
            defaultValue: defaultLEDBgColor,
            description: 'background color in hexadecimal.'}
        ],
        code: '{"type":"' + device.deviceDef.type +
        '","color":"' + defaultLEDColor +
        '","bgColor":"' + defaultLEDBgColor + '"}'
      };
    };
  });

  // register simple resistor
  $s.registerDevice('RES', function(device) {
    var in1 = device.addInput(); in1.setName('SRC');
    var out1 = device.addOutput(); out1.setName('TGT');
    var super_createUI = device.createUI;
    device.createUI = function() {
      super_createUI();
      var size = device.getSize();
      var $button = $s.createSVGElement('rect').
        attr({x: size.width * 3 / 8, y: size.height * 3 / 8,
          width: size.width / 4, height: size.height / 4,
          rx: 2, ry: 2});
      device.$ui.append($button);
      device.$ui.addClass('my1cir-res');
      device.$ui.on('inputValueChange', function() {
          out1.setValue(in1.getValue());
          if (in1.getValue()!==null) $button.addClass('my1cir-res-chk');
          else $button.removeClass('my1cir-res-chk');
      });
    };
  });

  var createSwitchFactory = function(type) {
    return function(device) {
      var in1 = device.addInput(); in1.setName('SRC');
      var out1 = device.addInOut(); out1.setName('TGT');
      var on = (type == 'PushOff');

      if (type == 'Toggle' && device.deviceDef.state) {
        on = device.deviceDef.state.on;
      }
      device.getState = function() {
        return type == 'Toggle'? { on : on } : null;
      };

      var updateOutput = function() {
        if (on) {
          out1.setOverride(in1.getValue());
          out1.setValue(in1.getValue());
        } else {
          out1.setOverride(null);
          out1.setValue(out1.getOutput()===null?
            null:out1.getOutput().getValue());
        }
      };
      updateOutput();

      var super_createUI = device.createUI;
      device.createUI = function() {
        super_createUI();
        var size = device.getSize();
        var $button = $s.createSVGElement('rect').
          attr({x: size.width / 4, y: size.height / 4,
            width: size.width / 2, height: size.height / 2,
            rx: 2, ry: 2});
        $button.addClass('my1cir-switch-button');
        if (type == 'Toggle' && on) {
          $button.addClass('my1cir-switch-button-pressed');
        }
        device.$ui.append($button);
        var button_mouseDownHandler = function(event) {
          event.preventDefault();
          event.stopPropagation();
          if (type == 'PushOn') {
            on = true;
            $button.addClass('my1cir-switch-button-pressed');
          } else if (type == 'PushOff') {
            on = false;
            $button.addClass('my1cir-switch-button-pressed');
          } else if (type == 'Toggle') {
            on = !on;
            $button.addClass('my1cir-switch-button-pressed');
          }
          updateOutput();
          $(document).on('mouseup', button_mouseUpHandler);
          $(document).on('touchend', button_mouseUpHandler);
        };
        var button_mouseUpHandler = function(event) {
          if (type == 'PushOn') {
            on = false;
            $button.removeClass('my1cir-switch-button-pressed');
          } else if (type == 'PushOff') {
            on = true;
            $button.removeClass('my1cir-switch-button-pressed');
          } else if (type == 'Toggle') {
            // keep state
            if (!on) {
              $button.removeClass('my1cir-switch-button-pressed');
            }
          }
          updateOutput();
          $(document).off('mouseup', button_mouseUpHandler);
          $(document).off('touchend', button_mouseUpHandler);
        };
        device.$ui.on('deviceAdd', function() {
          $s.enableEvents($button, true);
          $button.on('mousedown', button_mouseDownHandler);
          $button.on('touchstart', button_mouseDownHandler);
        });
        device.$ui.on('deviceRemove', function() {
          $s.enableEvents($button, false);
          $button.off('mousedown', button_mouseDownHandler);
          $button.off('touchstart', button_mouseDownHandler);
        });
        device.$ui.addClass('my1cir-switch');
      };
    };
  };

  // register switches
  $s.registerDevice('Switch', createSwitchFactory('Toggle'));
  $s.registerDevice('PushButton', createSwitchFactory('PushOn'));

  // symbol draw functions
  var drawBUF = function(g, x, y, width, height) {
    g.moveTo(x, y);
    g.lineTo(x + width, y + height / 2);
    g.lineTo(x, y + height);
    g.lineTo(x, y);
    g.closePath(true);
  };
  var drawAND = function(g, x, y, width, height) {
    g.moveTo(x, y);
    g.curveTo(x + width, y, x + width, y + height / 2);
    g.curveTo(x + width, y + height, x, y + height);
    g.lineTo(x, y);
    g.closePath(true);
  };
  var drawOR = function(g, x, y, width, height) {
    var depth = width * 0.2;
    g.moveTo(x, y);
    g.curveTo(x + width - depth, y, x + width, y + height / 2);
    g.curveTo(x + width - depth, y + height, x, y + height);
    g.curveTo(x + depth, y + height, x + depth, y + height / 2);
    g.curveTo(x + depth, y, x, y);
    g.closePath(true);
  };
  var drawXOR = function(g, x, y, width, height) {
    drawOR(g, x + 3, y, width - 3, height);
    var depth = (width - 3) * 0.2;
    g.moveTo(x, y + height);
    g.curveTo(x + depth, y + height, x + depth, y + height / 2);
    g.curveTo(x + depth, y, x, y);
    g.closePath();
  };
  var drawNOT = function(g, x, y, width, height) {
    drawBUF(g, x - 1, y, width - 2, height);
    g.drawCircle(x + width - 1, y + height / 2, 2);
  };
  var drawNAND = function(g, x, y, width, height) {
    drawAND(g, x - 1, y, width - 2, height);
    g.drawCircle(x + width - 1, y + height / 2, 2);
  };
  var drawNOR = function(g, x, y, width, height) {
    drawOR(g, x - 1, y, width - 2, height);
    g.drawCircle(x + width - 1, y + height / 2, 2);
  };
  var drawXNOR = function(g, x, y, width, height) {
    drawXOR(g, x - 1, y, width - 2, height);
    g.drawCircle(x + width - 1, y + height / 2, 2);
  };
  // logical functions - placed overrides to enable latch effect!
  var AND = function(a, b) { if (a==0||b==0) return 0; return a & b; };
  var OR = function(a, b) { if (a==1||b==1) return 1; return a | b; };
  var XOR = function(a, b) { return a ^ b; };
  var BUF = function(a) { return a==1?1:0; };
  var NOT = function(a) { return a==1?0:1; };

  var createLogicGateFactory = function(op, out, draw) {
    return function(device) {
      var in1 = device.addInput();
      var in2 = null;
      if (op != null) {
        in2 = device.addInput();
      }
      var vdc = device.addInput(); vdc.setName('VDC'); vdc.setAsPower();
      var gnd = device.addInput(); gnd.setName('GND'); gnd.setAsPower();
      var op1 = device.addOutput();
      device.$ui.on('inputValueChange', function() {
        var b = null;
        if (vdc.getValue()!==null&&gnd.getValue()!==null) {
          b = in1.getValue();
          if (in2!==null) {
            b = op(b,in2.getValue());
          }
        }
        if (b===null) op1.setValue(null);
        else op1.setValue(out(b)?1:0);
      });
      var super_createUI = device.createUI;
      device.createUI = function() {
        super_createUI();
        var size = device.getSize();
        var g = $s.graphics(device.$ui);
        g.attr['class'] = 'my1cir-symbol';
        draw(g,
          (size.width - unit) / 2,
          (size.height - unit) / 2,
          unit, unit);
      };
    };
  };

  if ($s.hide_gates===undefined) $s.hide_gates = true;
  // register logic gates (combinational logic)
  $s.registerDevice('BUF',createLogicGateFactory(null,BUF,drawBUF),$s.hide_gates);
  $s.registerDevice('NOT',createLogicGateFactory(null,NOT,drawNOT),$s.hide_gates);
  $s.registerDevice('AND',createLogicGateFactory(AND,BUF,drawAND),$s.hide_gates);
  $s.registerDevice('NAND',createLogicGateFactory(AND,NOT,drawNAND),$s.hide_gates);
  $s.registerDevice('OR',createLogicGateFactory(OR,BUF,drawOR),$s.hide_gates);
  $s.registerDevice('NOR',createLogicGateFactory(OR,NOT,drawNOR),$s.hide_gates);
  $s.registerDevice('XOR',createLogicGateFactory(XOR,BUF,drawXOR),$s.hide_gates);
  $s.registerDevice('XNOR',createLogicGateFactory(XOR,NOT,drawXNOR),$s.hide_gates);

  // from simcirjs
  var _7Seg = function() {
    var _SEGMENT_DATA = {
      a: [575, 138, 494, 211, 249, 211, 194, 137, 213, 120, 559, 120],
      b: [595, 160, 544, 452, 493, 500, 459, 456, 500, 220, 582, 146],
      c: [525, 560, 476, 842, 465, 852, 401, 792, 441, 562, 491, 516],
      d: [457, 860, 421, 892, 94, 892, 69, 864, 144, 801, 394, 801],
      e: [181, 560, 141, 789, 61, 856, 48, 841, 96, 566, 148, 516],
      f: [241, 218, 200, 453, 150, 500, 115, 454, 166, 162, 185, 145],
      g: [485, 507, 433, 555, 190, 555, 156, 509, 204, 464, 451, 464]
    };
    return {
      width: 636,
      height: 1000,
      allSegments: 'abcdefg',
      drawSegment: function(g, segment, color) {
        if (!color) {
          return;
        }
        var data = _SEGMENT_DATA[segment];
        var numPoints = data.length / 2;
        g.attr['fill'] = color;
        for (var i = 0; i < numPoints; i += 1) {
          var x = data[i * 2];
          var y = data[i * 2 + 1];
          if (i == 0) {
            g.moveTo(x, y);
          } else {
            g.lineTo(x, y);
          }
        }
        g.closePath(true);
      },
      drawPoint: function(g, color) {
        if (!color) {
          return;
        }
        g.attr['fill'] = color;
        g.drawCircle(542, 840, 46);
      }
    };
  }();

  var drawSeg = function(seg, g, pattern, hiColor, loColor, bgColor) {
    g.attr['stroke'] = 'none';
    if (bgColor) {
      g.attr['fill'] = bgColor;
      g.drawRect(0, 0, seg.width, seg.height);
    }
    var on;
    for (var i = 0; i < seg.allSegments.length; i += 1) {
      var c = seg.allSegments.charAt(i);
      on = (pattern != null && pattern.indexOf(c) != -1);
      seg.drawSegment(g, c, on? hiColor : loColor);
    }
    on = (pattern != null && pattern.indexOf('.') != -1);
    seg.drawPoint(g, on? hiColor : loColor);
  };

  var createSegUI = function(device, seg) {
    var size = device.getSize();
    var sw = seg.width;
    var sh = seg.height;
    var dw = size.width - unit;
    var dh = size.height - unit;
    var scale = (sw / sh > dw / dh)? dw / sw : dh / sh;
    var tx = (size.width - seg.width * scale) / 2;
    var ty = (size.height - seg.height * scale) / 2;
    return $s.createSVGElement('g').
      attr('transform', 'translate(' + tx + ' ' + ty + ')' +
          ' scale(' + scale + ') ');
  };

  var segCC = 0; // common cathode
  var segCA = 1; // common anode

  var createLEDSegFactory = function(seg,type) {
    return function(device) {
      var hiColor = device.deviceDef.color || defaultLEDColor;
      var bgColor = device.deviceDef.bgColor || defaultSEGBgColor;
      var loColor = multiplyColor(hiColor, bgColor, 0.25);
      var allSegs = seg.allSegments + '.';
      //device.halfPitch = true;
      device.oneSided = true;
      for (var i = 0; i < allSegs.length; i += 1) {
        var chk = device.addInput();
        var lbl = allSegs.charAt(i);
        if (lbl==='.') lbl = "dp";
        chk.setName(lbl);
      }
      var segType = segCC;
      var segName = 'COM-C';
      if (type==='Anode') {
        segType = segCA;
        segName = 'COM-A';
      }
      var com = device.addInput(); com.setName(segName);
      var super_getSize = device.getSize;
      device.getSize = function() {
        var size = super_getSize();
        return {width: unit * 4, height: size.height};
      };

      var super_createUI = device.createUI;
      device.createUI = function() {
        super_createUI();

        var $seg = createSegUI(device, seg);
        device.$ui.append($seg);

        var update = function() {
          var segs = '';
          for (var i = 0; i < allSegs.length; i += 1) {
            var temp = device.getInputs()[i].getValue();
            var chk1 = com.getValue();
            if (chk1!==segType) chk1 = temp;
            if (temp!==null&&temp!==chk1) {
              segs += allSegs.charAt(i);
            }
          }
          $seg.children().remove();
          drawSeg(seg, $s.graphics($seg), segs,
              hiColor, loColor, bgColor);
        };
        device.$ui.on('inputValueChange', update);
        update();
        device.doc = {
          params: [
            {name: 'color', type: 'string',
              defaultValue: defaultLEDColor,
              description: 'color in hexadecimal.'},
            {name: 'bgColor', type: 'string',
              defaultValue: defaultLEDBgColor,
              description: 'background color in hexadecimal.'}
          ],
          code: '{"type":"' + device.deviceDef.type +
          '","color":"' + defaultLEDColor +
          '","bgColor":"' + defaultLEDBgColor + '"}'
        };
      };
    };
  };

  // register 7-segment... useful to have
  $s.registerDevice('7seg-CA',createLEDSegFactory(_7Seg,'Anode'),$s.hide_gates);
  $s.registerDevice('7seg-CC',createLEDSegFactory(_7Seg,'Cathode'),$s.hide_gates);

}(simcir);
