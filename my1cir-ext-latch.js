// my1cir-ext-latch
// - latches
// - requires my1cir-ext

// need an oscillator to test sequential circuits (counters?)

simcir.registerDevice('OSC', function(device) {
  var defOSCfreq = 2; // in Hz?
  var freq = device.deviceDef.freq || defOSCfreq;
  var delay = ~~(500 / freq); // in ms?
  var out1 = device.addOutput();
  var timerId = null;
  var on = 0;
  device.$ui.on('deviceAdd', function() {
    timerId = window.setInterval(function() {
      on = on==1?0:1;
      out1.setValue(on);
    }, delay);
  });
  device.$ui.on('deviceRemove', function() {
    if (timerId != null) {
      window.clearInterval(timerId);
      timerId = null;
    }
  });
  var super_createUI = device.createUI;
  device.createUI = function() {
    super_createUI();
    device.$ui.addClass('my1cir-osc');
    device.doc = {
      params: [
        {name: 'freq', type: 'number', defaultValue: defOSCfreq,
          description: 'frequency of an oscillator (Hz)'}
      ],
      code: '{"type":"' + device.deviceDef.type + '","freq":'+defOSCfreq+'}'
    };
  };
});

// register latches (sequential logic)

simcir.registerDevice('SR-Latch',{
  "width":400,
  "height":300,
  "showToolbox":false,
  "toolbox":[
  ],
  "layout":{
    "rows":10,"cols":8,"hideLabelOnWorkspace":true,
    "nodes":{
    "VDC":"T4","GND":"B4",
    "~S":"L2","~R":"L8",
    "Q":"R2","~Q":"R8"
    }
  },
  "devices":[
    {"type":"NAND","id":"dev0","x":168,"y":64,"label":"NAND"},
    {"type":"NAND","id":"dev1","x":168,"y":168,"label":"NAND"},
    {"type":"In","id":"dev2","x":64,"y":112,"label":"~R"},
    {"type":"In","id":"dev3","x":64,"y":56,"label":"~S"},
    {"type":"In","id":"dev4","x":64,"y":168,"label":"VDC"},
    {"type":"In","id":"dev5","x":64,"y":224,"label":"GND"},
    {"type":"Out","id":"dev6","x":232,"y":80,"label":"Q"},
    {"type":"Out","id":"dev7","x":232,"y":184,"label":"~Q"}
  ],
  "connectors":[
    {"from":"dev0.in0","to":"dev3.out0"},
    {"from":"dev0.in1","to":"dev1.out0"},
    {"from":"dev0.in2","to":"dev4.out0"},
    {"from":"dev0.in3","to":"dev5.out0"},
    {"from":"dev1.in0","to":"dev2.out0"},
    {"from":"dev1.in1","to":"dev0.out0"},
    {"from":"dev1.in2","to":"dev4.out0"},
    {"from":"dev1.in3","to":"dev5.out0"},
    {"from":"dev6.in0","to":"dev0.out0"},
    {"from":"dev7.in0","to":"dev1.out0"}
  ]
});

simcir.registerDevice('D-Latch',{
  "width":400,
  "height":300,
  "showToolbox":false,
  "toolbox":[
  ],
  "layout":{
    "rows":10,"cols":8,"hideLabelOnWorkspace":true,
    "nodes":{
    "VDC":"T4","GND":"B4",
    "D":"L2","G":"L8",
    "Q":"R2","~Q":"R8"
    }
  },
  "devices":[
    {"type":"SR-Latch","id":"dev0","x":232,"y":80,"label":"SR-LATCH"},
    {"type":"Out","id":"dev1","x":328,"y":80,"label":"Q"},
    {"type":"Out","id":"dev2","x":328,"y":128,"label":"~Q"},
    {"type":"In","id":"dev3","x":40,"y":128,"label":"VDC"},
    {"type":"In","id":"dev4","x":40,"y":176,"label":"GND"},
    {"type":"In","id":"dev5","x":40,"y":32,"label":"D"},
    {"type":"In","id":"dev6","x":40,"y":80,"label":"G"},
    {"type":"NAND","id":"dev7","x":168,"y":40,"label":"NAND"},
    {"type":"NAND","id":"dev8","x":168,"y":128,"label":"NAND"},
    {"type":"NOT","id":"dev9","x":104,"y":168,"label":"NOT"}
  ],
  "connectors":[
    {"from":"dev0.in0","to":"dev8.out0"},
    {"from":"dev0.in1","to":"dev7.out0"},
    {"from":"dev0.in2","to":"dev3.out0"},
    {"from":"dev0.in3","to":"dev4.out0"},
    {"from":"dev1.in0","to":"dev0.out0"},
    {"from":"dev2.in0","to":"dev0.out1"},
    {"from":"dev7.in0","to":"dev5.out0"},
    {"from":"dev7.in1","to":"dev6.out0"},
    {"from":"dev7.in2","to":"dev3.out0"},
    {"from":"dev7.in3","to":"dev4.out0"},
    {"from":"dev8.in0","to":"dev9.out0"},
    {"from":"dev8.in1","to":"dev6.out0"},
    {"from":"dev8.in2","to":"dev3.out0"},
    {"from":"dev8.in3","to":"dev4.out0"},
    {"from":"dev9.in0","to":"dev5.out0"},
    {"from":"dev9.in1","to":"dev3.out0"},
    {"from":"dev9.in2","to":"dev4.out0"}
  ]
});

simcir.registerDevice('D-FF',{
  "width":400,
  "height":300,
  "showToolbox":false,
  "toolbox":[
  ],
  "layout":{
    "rows":10,"cols":8,"hideLabelOnWorkspace":true,
    "nodes":{
    "VDC":"T4","GND":"B4",
    "D":"L2","CK":"L8",
    "Q":"R2","~Q":"R8"
    }
  },
  "devices":[
    {"type":"D-Latch","id":"dev0","x":256,"y":48,"label":"D-Latch"},
    {"type":"In","id":"dev1","x":24,"y":120,"label":"VDC"},
    {"type":"In","id":"dev2","x":24,"y":168,"label":"GND"},
    {"type":"In","id":"dev3","x":24,"y":24,"label":"D"},
    {"type":"In","id":"dev4","x":24,"y":72,"label":"CK"},
    {"type":"NOT","id":"dev5","x":104,"y":152,"label":"NOT"},
    {"type":"D-Latch","id":"dev6","x":160,"y":48,"label":"D-Latch"},
    {"type":"Out","id":"dev7","x":344,"y":48,"label":"Q"},
    {"type":"Out","id":"dev8","x":344,"y":96,"label":"~Q"}
  ],
  "connectors":[
    {"from":"dev0.in0","to":"dev1.out0"},
    {"from":"dev0.in1","to":"dev2.out0"},
    {"from":"dev0.in2","to":"dev6.out0"},
    {"from":"dev0.in3","to":"dev4.out0"},
    {"from":"dev5.in0","to":"dev4.out0"},
    {"from":"dev5.in1","to":"dev1.out0"},
    {"from":"dev5.in2","to":"dev2.out0"},
    {"from":"dev6.in0","to":"dev1.out0"},
    {"from":"dev6.in1","to":"dev2.out0"},
    {"from":"dev6.in2","to":"dev3.out0"},
    {"from":"dev6.in3","to":"dev5.out0"},
    {"from":"dev7.in0","to":"dev0.out0"},
    {"from":"dev8.in0","to":"dev0.out1"}
  ]
});

simcir.registerDevice('SR-Latch-PC',{
  "width":400,
  "height":300,
  "showToolbox":false,
  "toolbox":[
  ],
  "layout":{
    "rows":10,"cols":10,"hideLabelOnWorkspace":true,
    "nodes":{
    "VDC":"T4","GND":"B4",
    "~P":"T7","~C":"B7",
    "~S":"L2","~R":"L8",
    "Q":"R2","~Q":"R8"
    }
  },
  "devices":[
    {"type":"In","id":"dev0","x":32,"y":168,"label":"~R"},
    {"type":"In","id":"dev1","x":32,"y":224,"label":"VDC"},
    {"type":"In","id":"dev2","x":32,"y":120,"label":"~S"},
    {"type":"In","id":"dev3","x":32,"y":272,"label":"GND"},
    {"type":"Out","id":"dev4","x":304,"y":184,"label":"~Q"},
    {"type":"In","id":"dev5","x":32,"y":24,"label":"~P"},
    {"type":"In","id":"dev6","x":32,"y":72,"label":"~C"},
    {"type":"NOT","id":"dev7","x":96,"y":192,"label":"NOT"},
    {"type":"NAND","id":"dev8","x":144,"y":192,"label":"NAND"},
    {"type":"NAND","id":"dev9","x":144,"y":64,"label":"NAND"},
    {"type":"AND","id":"dev10","x":200,"y":64,"label":"AND"},
    {"type":"Out","id":"dev11","x":304,"y":96,"label":"Q"},
    {"type":"NAND","id":"dev12","x":256,"y":80,"label":"NAND"},
    {"type":"AND","id":"dev13","x":200,"y":192,"label":"AND"},
    {"type":"NOT","id":"dev14","x":96,"y":64,"label":"NOT"},
    {"type":"NAND","id":"dev15","x":256,"y":176,"label":"NAND"}
  ],
  "connectors":[
    {"from":"dev4.in0","to":"dev15.out0"},
    {"from":"dev7.in0","to":"dev0.out0"},
    {"from":"dev7.in1","to":"dev1.out0"},
    {"from":"dev7.in2","to":"dev3.out0"},
    {"from":"dev8.in0","to":"dev5.out0"},
    {"from":"dev8.in1","to":"dev7.out0"},
    {"from":"dev8.in2","to":"dev1.out0"},
    {"from":"dev8.in3","to":"dev3.out0"},
    {"from":"dev9.in0","to":"dev6.out0"},
    {"from":"dev9.in1","to":"dev14.out0"},
    {"from":"dev9.in2","to":"dev1.out0"},
    {"from":"dev9.in3","to":"dev3.out0"},
    {"from":"dev10.in0","to":"dev5.out0"},
    {"from":"dev10.in1","to":"dev9.out0"},
    {"from":"dev10.in2","to":"dev1.out0"},
    {"from":"dev10.in3","to":"dev3.out0"},
    {"from":"dev11.in0","to":"dev12.out0"},
    {"from":"dev12.in0","to":"dev10.out0"},
    {"from":"dev12.in1","to":"dev15.out0"},
    {"from":"dev12.in2","to":"dev1.out0"},
    {"from":"dev12.in3","to":"dev3.out0"},
    {"from":"dev13.in0","to":"dev6.out0"},
    {"from":"dev13.in1","to":"dev8.out0"},
    {"from":"dev13.in2","to":"dev1.out0"},
    {"from":"dev13.in3","to":"dev3.out0"},
    {"from":"dev14.in0","to":"dev2.out0"},
    {"from":"dev14.in1","to":"dev1.out0"},
    {"from":"dev14.in2","to":"dev3.out0"},
    {"from":"dev15.in0","to":"dev12.out0"},
    {"from":"dev15.in1","to":"dev13.out0"},
    {"from":"dev15.in2","to":"dev1.out0"},
    {"from":"dev15.in3","to":"dev3.out0"}
  ]
});

simcir.registerDevice('D-Latch-PC',{
  "width":400,
  "height":300,
  "showToolbox":false,
  "toolbox":[
  ],
  "layout":{
    "rows":10,"cols":10,"hideLabelOnWorkspace":true,
    "nodes":{
    "VDC":"T4","GND":"B4",
    "~P":"T7","~C":"B7",
    "D":"L2","G":"L8",
    "Q":"R2","~Q":"R8"
    }
  },
  "devices":[
    {"type":"In","id":"dev0","x":32,"y":216,"label":"VDC"},
    {"type":"In","id":"dev1","x":32,"y":264,"label":"GND"},
    {"type":"In","id":"dev2","x":32,"y":120,"label":"D"},
    {"type":"In","id":"dev3","x":32,"y":168,"label":"G"},
    {"type":"NAND","id":"dev4","x":176,"y":88,"label":"NAND"},
    {"type":"NAND","id":"dev5","x":176,"y":176,"label":"NAND"},
    {"type":"NOT","id":"dev6","x":112,"y":216,"label":"NOT"},
    {"type":"Out","id":"dev7","x":344,"y":120,"label":"Q"},
    {"type":"Out","id":"dev8","x":344,"y":168,"label":"~Q"},
    {"type":"SR-Latch-PC","id":"dev9","x":240,"y":120,"label":"SR-Latch-PC"},
    {"type":"In","id":"dev10","x":32,"y":72,"label":"~C"},
    {"type":"In","id":"dev11","x":32,"y":24,"label":"~P"}
  ],
  "connectors":[
    {"from":"dev4.in0","to":"dev2.out0"},
    {"from":"dev4.in1","to":"dev3.out0"},
    {"from":"dev4.in2","to":"dev0.out0"},
    {"from":"dev4.in3","to":"dev1.out0"},
    {"from":"dev5.in0","to":"dev6.out0"},
    {"from":"dev5.in1","to":"dev3.out0"},
    {"from":"dev5.in2","to":"dev0.out0"},
    {"from":"dev5.in3","to":"dev1.out0"},
    {"from":"dev6.in0","to":"dev2.out0"},
    {"from":"dev6.in1","to":"dev0.out0"},
    {"from":"dev6.in2","to":"dev1.out0"},
    {"from":"dev7.in0","to":"dev9.out1"},
    {"from":"dev8.in0","to":"dev9.out0"},
    {"from":"dev9.in0","to":"dev5.out0"},
    {"from":"dev9.in1","to":"dev0.out0"},
    {"from":"dev9.in2","to":"dev4.out0"},
    {"from":"dev9.in3","to":"dev1.out0"},
    {"from":"dev9.in4","to":"dev11.out0"},
    {"from":"dev9.in5","to":"dev10.out0"}
  ]
});

simcir.registerDevice('D-FF-PC',{
  "width":400,
  "height":300,
  "showToolbox":false,
  "toolbox":[
  ],
  "layout":{
    "rows":10,"cols":10,"hideLabelOnWorkspace":true,
    "nodes":{
    "VDC":"T4","GND":"B4",
    "~P":"T7","~C":"B7",
    "D":"L2","CK":"L8",
    "Q":"R2","~Q":"R8"
    }
  },
  "devices":[
    {"type":"In","id":"dev0","x":24,"y":208,"label":"VDC"},
    {"type":"In","id":"dev1","x":24,"y":256,"label":"GND"},
    {"type":"In","id":"dev2","x":24,"y":112,"label":"D"},
    {"type":"In","id":"dev3","x":24,"y":160,"label":"CK"},
    {"type":"NOT","id":"dev4","x":88,"y":192,"label":"NOT"},
    {"type":"D-Latch","id":"dev5","x":144,"y":88,"label":"D-Latch"},
    {"type":"Out","id":"dev6","x":328,"y":88,"label":"Q"},
    {"type":"Out","id":"dev7","x":328,"y":136,"label":"~Q"},
    {"type":"In","id":"dev8","x":24,"y":64,"label":"~C"},
    {"type":"In","id":"dev9","x":24,"y":16,"label":"~P"},
    {"type":"D-Latch-PC","id":"dev10","x":232,"y":88,"label":"D-Latch-PC"}
  ],
  "connectors":[
    {"from":"dev4.in0","to":"dev3.out0"},
    {"from":"dev4.in1","to":"dev0.out0"},
    {"from":"dev4.in2","to":"dev1.out0"},
    {"from":"dev5.in0","to":"dev0.out0"},
    {"from":"dev5.in1","to":"dev1.out0"},
    {"from":"dev5.in2","to":"dev2.out0"},
    {"from":"dev5.in3","to":"dev4.out0"},
    {"from":"dev6.in0","to":"dev10.out0"},
    {"from":"dev7.in0","to":"dev10.out1"},
    {"from":"dev10.in0","to":"dev0.out0"},
    {"from":"dev10.in1","to":"dev1.out0"},
    {"from":"dev10.in2","to":"dev5.out0"},
    {"from":"dev10.in3","to":"dev3.out0"},
    {"from":"dev10.in4","to":"dev8.out0"},
    {"from":"dev10.in5","to":"dev9.out0"}
  ]
});
