#!/bin/bash

XINC=32
XLEN=27
(( XEND = XLEN - 1 ))

echo "  \"devices\":["
DOFF=0
YPOS=32
XPOS=24
for (( a=0;a<$XLEN;a++ )); do
	(( c = a + DOFF ))
	echo -n "    {\"type\":\"Joint\",\"id\":\"dev${c}\","
	echo -n "\"x\":$XPOS,\"y\":$YPOS,\"label\":\"Joint\","
	echo -n "\"state\":{\"direction\":0}}"
	echo -n ","
	echo
	(( XPOS = XPOS + XINC ))
done
(( DOFF = DOFF + XLEN ))
YPOS=480
XPOS=24
for (( a=0;a<$XLEN;a++ )); do
	(( c = a + DOFF ))
	echo -n "    {\"type\":\"Joint\",\"id\":\"dev${c}\","
	echo -n "\"x\":$XPOS,\"y\":$YPOS,\"label\":\"Joint\","
	echo -n "\"state\":{\"direction\":0}}"
	[ $a -lt $XEND ] && echo -n ","
	echo
	(( XPOS = XPOS + XINC ))
done
echo "  ],"

echo "  \"connectors\":["
DOFF=0
for (( a=0;a<$XEND;a++ )); do
	(( c = a + DOFF ))
	(( b = a + 1 ))
	(( d = b + DOFF ))
	echo -n "    {\"from\":\"dev${d}.in0\",\"to\":\"dev${c}.out0\"}"
	echo -n ","
	echo
done
(( DOFF = DOFF + XLEN ))
for (( a=0;a<$XEND;a++ )); do
	(( c = a + DOFF ))
	(( b = a + 1 ))
	(( d = b + DOFF ))
	echo -n "    {\"from\":\"dev${d}.in0\",\"to\":\"dev${c}.out0\"}"
	[ $b -lt $XEND ] && echo -n ","
	echo
done
echo "  ]"

# sh create_strip.sh 72 104 32 10 10
# - creates 10 connected jointios
# - at ypos=72 starting from xpos=104
# - 32 pixels apart? start at dev10
