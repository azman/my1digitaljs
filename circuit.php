<?php
define('ERRORAPI','Invalid API!');
define('DEBUGAPI',false);
define('HTMLVIEW','circuit.html');
define('HTMLVIE7','circuit7.html');
define('HTMLDEVS','developer.html');
function error_exit($message) {
	if (empty($message)||DEBUGAPI===false)
		$message = ERRORAPI;
	throw new Exception($message);
}
try {
	$method = 'GET';
	if (isset($_SERVER['REQUEST_METHOD'])) {
		$method = $_SERVER['REQUEST_METHOD'];
	}
	$target = $_SERVER['REQUEST_URI'];
	$chksrv = dirname($target);
	$topchk = dirname($_SERVER['SCRIPT_FILENAME']);
	$topchk = explode('/', $topchk);
	$topchk = array_pop($topchk);
	$params = explode('/', $target);
	do {
		$chkchk = array_shift($params); // find api top folder
		if ($chkchk===false) {
			error_exit("Cannot find API top folder!");
		}
	} while($chkchk!=$topchk);
	$dohtml = HTMLVIEW;
	// get developer option
	$option = array_shift($params);
	$option = strtolower($option);
	if (!empty($option)) {
		$chksrv = dirname($chksrv);
		if ($option==="developer") {
			$dohtml = HTMLDEVS;
		} else if ($option==="circuit7") {
			$dohtml = HTMLVIE7;
		} else {
			error_exit("Invalid option '".$option."'!");
		}
	}
	if ($chksrv==="/") $chksrv = "";
	// params should be empty by now
	if (!empty($params)) {
		error_exit("Invalid params!");
	}
	// check request
	switch ($method) {
	case 'GET':
		// get viewer html
		if (!file_exists($dohtml)) {
			error_exit("Viewer page NOT found!");
		}
		// send viewer template
		$doview = file_get_contents($dohtml);
		// can do some processing here...
		echo $doview;
		exit();
		break;
	case 'POST':
		// data post = submission?
		error_exit('Nothing yet!');
		break;
	default:
		error_exit('Invalid Request!');
	}
} catch (Exception $errmsg) {
	// uncomment the next 2 lines to clean up session on error?
	session_start();
	session_destroy();
	header("HTTP/1.0 404 Not Found");
	header('Content-Type: text/html; charset=utf-8');
	echo "<h1>ERROR: ".$errmsg->getMessage()."</h1>\n";
}
exit();
?>
